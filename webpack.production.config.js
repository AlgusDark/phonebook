var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

var PATHS = {
  src: path.join(__dirname, 'src'),
  scenes: path.join(__dirname, 'src/scenes'),
  dist: path.join(__dirname, 'dist')
};

var config = {
  devtool: 'eval-source-map',

  entry: PATHS.src + '/index.jsx',

  output: {
    path: PATHS.dist,
    filename: "[name]-[hash].js"
  },

  resolve: {
    extensions: ['', '.js', '.jsx']
  },

  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel'
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?modules!postcss')
      }
    ]
  },

  postcss: [
    require('autoprefixer')
  ],

  plugins: [
    new HtmlWebpackPlugin({
      template: PATHS.src + '/index.tmpl.html',
      inject: 'body',
      filename: 'index.html',
      title: 'My PhoneBook App'
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractTextPlugin("[name]-[hash].css")
  ]
}

module.exports = config;
