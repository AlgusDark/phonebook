import React from 'react';
import { Link } from 'react-router';

import Loader from '../../../../components/Loader';

import style from './style.css';

const ContactList = ({ contacts }) => {
  if (contacts.length === 0) return <section><div>No contacts Yet</div></section>;

  return(
    <ul className={style.contacts}>
      {contacts.map((contact, id) =>
        <li key={id}>
          <img src={contact.picture} />
          <div className={style.data}>
            <p><strong>Name:</strong> {contact.name}</p>
            <p><strong>Phone:</strong> {contact.cell}</p>
          </div>
          <div className={style.edit}><Link to={`contacts/edit/${id}`}>Edit</Link></div>
        </li>
      )}
    </ul>
  )
}

export default Loader('isFetching')(ContactList);
