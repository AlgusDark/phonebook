import React, {Component} from 'react';
import { connect } from 'react-redux';

import * as actions from '../../data/contacts/actions'

import ContactList from './components/ContactList';

class Contacts extends Component {
  componentDidMount() {
    const { contacts, isFetching, fetchContacts } = this.props;
    if (contacts.length === 0 && isFetching === false) {
        fetchContacts();
    }
  }

  render() {
    return (
        <ContactList {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.contacts
  };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchContacts: () => dispatch(actions.fetchContacts())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
