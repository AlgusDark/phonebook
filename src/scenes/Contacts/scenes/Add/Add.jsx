import React, {Component} from 'react';
import { connect } from 'react-redux';

import * as actions from '../../../../data/contacts/actions';
import AddContact from './components/AddContact';

const Add = (props) => {
    const handleAddContact = ({name, cell}) => {
      const contact = {
        name,
        cell,
        picture: 'http://i.imgur.com/bYyts3u.jpg'
      };

      props.dispatch(actions.addContact(contact))
    }

    return (
      <section>
        <AddContact {...props} addContact={handleAddContact} />
      </section>
    )
}

export default connect()(Add);
