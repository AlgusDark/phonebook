import React from 'react';

import style from './style.css';

const AddContact = ({ contacts, addContact = f => f }) => {
  let name, cell;

  const handleAddContact = e => {
    e.preventDefault();
    addContact({
        name: name.value, 
        cell: cell.value
    });

    name.value = '';
    cell.value = '';
  }

  return (
    <section className={style.control}>
      <input type="text" placeholder="Name" name="name" ref={ element => name = element }/>
      <input type="text" placeholder="Cellphone" name="cell" ref={ element => cell = element }/>
      <button onClick={handleAddContact}>Add Contact</button>
    </section>
  )
}

export default AddContact;
