import React from 'react';
import { connect } from 'react-redux';

const Edit = (props) => {
    const contactID = +props.params.contactID;
    const contact = props.contacts.contacts[contactID];

    let name,cell;

    return (
        <section>
          <input type="text" defaultValue={contact.name} placeholder="Name" name="name" ref={ element => name = element }/>
          <input type="text" defaultValue={contact.cell} placeholder="Cellphone" name="cell" ref={ element => cell = element }/>
          <button>Edit Contact</button>
        </section>
    )
}

const mapStateToProps = (state) => {
    return {
        contacts: {...state.contacts}
    }
}

export default connect(mapStateToProps)(Edit);
