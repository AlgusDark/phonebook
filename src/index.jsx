import 'babel-polyfill';

import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import store from './store';

import Main from './components/Main';

import Contacts from './scenes/Contacts';
import Add from './scenes/Contacts/scenes/Add';
import Edit from './scenes/Contacts/scenes/Edit';

render((
<Provider store={store}>
  <Router history={hashHistory}>
    <Route path="/" component={Main}>
        <IndexRoute component={Contacts}/>
        <Route path="contacts">
            <IndexRoute component={Contacts}/>
            <Route path="add" component={Add}/>
            <Route path="edit/:contactID" component={Edit}/>
        </Route>
    </Route>
  </Router>
</Provider>
), document.getElementById('root'))
