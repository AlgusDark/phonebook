import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk'

import {reducer as contacts} from './data/contacts/reducer';

// Combine Reducers
const rootReducer = combineReducers({
  contacts
});

const middleware = applyMiddleware(
    thunkMiddleware
);

const store = createStore(rootReducer, middleware);

export default store;
