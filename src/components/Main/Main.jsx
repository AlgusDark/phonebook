import React, {Component} from 'react';

import NavBar from '../NavBar';

const Main = (props) => {
    return (
        <section>
          <NavBar/>
          {props.children}
        </section>
    )
}

export default Main;
