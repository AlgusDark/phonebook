import React from 'react';
import { Link } from 'react-router'

import styles from './styles.css';

const NavLink = (props) => {
    return <Link {...props} activeClassName={styles.active} />
}

const NavBar = () => {
    return(
        <nav role="navigation" className={styles.nav}>
            <NavLink onlyActiveOnIndex={true} to="/">Home</NavLink>
            <NavLink to="/contacts/add">Add Contact</NavLink>
        </nav>
    )
}

export default NavBar;
