import * as actionTypes from './actionTypes';

const initialState = {
    contacts: [],
    isFetching: false
}

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CONTACTS_REQUEST:
            return {...state, isFetching: true};
        case actionTypes.FETCH_CONTACTS_FAILURE:
            return {...state, isFetching: false};
        case actionTypes.FETCH_CONTACTS_SUCCESS:
            return {...state, contacts: action.contacts, isFetching: false};
        case actionTypes.ADD_CONTACT:
            return {...state, contacts: [...state.contacts, action.contact]};
        default:
            return state;
    }
}
