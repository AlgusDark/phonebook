export const FETCH_CONTACTS_REQUEST = Symbol();
export const FETCH_CONTACTS_FAILURE = Symbol();
export const FETCH_CONTACTS_SUCCESS = Symbol();

export const ADD_CONTACT = Symbol();
