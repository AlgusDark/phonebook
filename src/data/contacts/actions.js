import * as actionTypes from './actionTypes';
import axios from 'axios';

export const fetchContactsRequest = () => (
    { type: actionTypes.FETCH_CONTACTS_REQUEST }
)

export const fetchContactsFailure = () => (
    { type: actionTypes.FETCH_CONTACTS_FAILURE }
)

export const fetchContactsSuccess = (contacts) => (
    { type: actionTypes.FETCH_CONTACTS_SUCCESS, contacts }
)

export const addContact = (contact) => (
    { type: actionTypes.ADD_CONTACT, contact }
)

export function fetchContacts() {
    return dispatch => {
        dispatch(fetchContactsRequest());

        return axios.get('https://api.randomuser.me/?inc=name,cell,picture&results=5')
            .then(response => {
                const contacts = response.data.results.map( ({ name, cell, picture }) => ({
                    name: `${name.first} ${name.last}`,
                    picture: picture.medium,
                    cell
                }));

                return dispatch(fetchContactsSuccess(contacts));
            })
            .catch(e => {
                dispatch(fetchContactsFailure());
            });
    }
}
