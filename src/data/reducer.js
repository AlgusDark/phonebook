import { combineReducers } from 'redux';
import { reducer as contacts } from './contacts/reducer';

export const reducer = combineReducers({
    contacts
});
